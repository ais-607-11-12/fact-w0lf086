/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>

using namespace std;


int factorial(int N)
{
	int F = 1;
	
	for(int i=1; i<=N; ++i)
	{
		F *= i;
	}
	return F;
}


int main()
{
	int N;
	cout << "N = ";
	cin >> N;
	
	if(N >= 0)
	{
		cout << factorial(N); 
	}
	else
	{
		cout << "Error: N < 0.";
	}
	
	return 0;
}
