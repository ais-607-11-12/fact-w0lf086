Предметная область: Заказ


|Термин|Определение|Синоним|
|----|----|----|		
|Заказчик	|лицо (физическое или юридическое), заинтересованное в выполнении исполнителем работ, оказании им услуг или приобретении у продавца какого-либо продукта (в широком смысле). Иногда при этом предполагается оформление заказа, но не обязательно.	|клиент, потребитель, давалец, наниматель, покупатель |
|Заказ	|поручение на изготовление чего-либо или оказание услуги|Задание,госзаказ,миссия,поручение,спецзаказ  |
|Сотрудник	|Субъект трудового права, физическое лицо, работающее по трудовому договору у работодателя и получающее за это заработную плату. 	|Ассистент,персонал,подручный,помощник,пособник,правдист,представитель|
|Отдел	|подразделение учреждения	| |
|Документ	|зафиксированная на материальном носителе информация в виде текста, звукозаписи или изображения с реквизитами, позволяющими её идентифицировать| 	Протокол, квитанция |
|Договор 	|соглашение между собой двух или более сторон (субъектов), по какому-либо вопросу с целью установления, изменения или прекращения правовых отношений	|соглашение, условность, условие, конвенция, трактат, контракт, пакт, соглашение, уговор, завет, договоренность, (торговая) сделка, стачка, обязательство  |
|Платежный документ	|группа первичных документов позволяющий подтвердить факт оплаты приобретаемых товаров или услуг| Чек |
|Смета| документ, в котором рассчитывают затраты на проект исходя из расходов: на работы, стройматериалы, хозяйственные нужды, приобретение комплектующих и прочее.|План, бюджет, расчет, соображение, роспись, сметка, выметка, распределение |
|Материал| вещество или смесь веществ, из которых изготавливается продукция.| |
|Поставщик| любое юридическое или физическое лицо, поставляющее товары или услуги заказчикам. Поставщик осуществляет предпринимательскую деятельность в соответствии с условиями заключённого договора поставки, который является одним из видов договора купли-продажи.| Подрядчик,шипчандлер,генпоставщик,лесопоставщик,снабженец,импортер,выстановщик,поставитель,поставлятель,заготовщик,готовщик|
|Склад| территория, помещение (также их комплекс), предназначенное для хранения материальных ценностей и оказания складских услуг.|Хранилище, база, каптерка |
